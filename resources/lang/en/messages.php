<?php

return [

    /*
    |--------------------------------------------------------------------------
    | common(title/nav)
    |--------------------------------------------------------------------------
    */

    'corporation_type' => 'Inc.',
    'title_news' => 'NEWS',
    'title_bbs' => 'BBS',
    'title_company' => 'Company Profile',

    /*
    |--------------------------------------------------------------------------
    | views/home/index.blade.php
    |--------------------------------------------------------------------------
    */

    'index_menu' => 'Menu',
    'index_news' => 'News',
    'index_contact' => 'Contact',

    /*
    |--------------------------------------------------------------------------
    | views/home/news.blade.php
    |--------------------------------------------------------------------------
    */

    'news_add' => 'New addition',
    'news_title' => 'Title',
    'news_content' => 'Text',
    'news_delete' => 'Delete',


    /*
    |--------------------------------------------------------------------------
    | views/home/top.blade.php
    |--------------------------------------------------------------------------
    */

    'top_recent' => 'Recent',
    'top_contact' => 'Contact',
    'top_name' => 'Name',
    'top_email' => 'Email',
    'top_type' => 'Category',
    'top_type_service' => 'service',
    'top_type_acceptance' => 'acceptance',
    'top_type_other' => 'other',
    'top_content' => 'Message',
    'top_sent' => 'Submit',

    /*
    |--------------------------------------------------------------------------
    | views/home/company.blade.php
    |--------------------------------------------------------------------------
    */

    'company_name_title' => 'Company Name',
    'company_name' => 'AD Inc.',

    'company_address_title' => 'Office',
    'company_address' => 'Tokyo',

    'company_establishment_title' => 'Establishment',
    'company_establishment' => 'May 11, 2018',

    'company_officer_title' => 'officer',
    'company_officer' => 'CEO Taro Yamada',

    'company_business_title' => 'Business',
    'company_business' => 'Internet Service',

    'company_employees_title' => 'Number of Employees',
    'company_employees' => '51',

    'company_bank_title' => 'Bank',
    'company_bank' => 'Dummy Bank',

    'company_motto_title' => 'Motto',
    'company_motto' => 'Wer mit Ungeheuern kämpft, mag zusehn, dass er nicht dabei zum Ungeheuer wird. Und wenn du lange in einen Abgrund blickst, blickt der Abgrund auch in dich hinein.',

    /*
    |--------------------------------------------------------------------------
    | views/home/contact.blade.php
    |--------------------------------------------------------------------------
    */

    'contact_type_service' => 'about service',
    'contact_type_recruit' => 'about acceptance',
    'contact_type_other' => 'other',
    'contact_back' => 'back',
    'contact_next' => 'next',


    /*
    |--------------------------------------------------------------------------
    | views/home/submit.blade.php
    |--------------------------------------------------------------------------
    */

    'bbs_post' => 'bbs posting',
    'bbs_discription' => 'Fill in the following blanks',
    'bbs_name' => 'Name',
    'bbs_title' => 'Title',
    'bbs_file' => 'File',
    'bbs_content' => 'Message',
    'bbs_sent' => 'Submit',

    /*
    |--------------------------------------------------------------------------
    | views/home/list.blade.php
    |--------------------------------------------------------------------------
    */

    'list_to_post' => 'To posting page',
    'list_add' => 'Add',
    'list_comment_title' => 'Comment',
    'list_comment_post' => 'Post',
    'list_name' => 'Name',
    'list_content' => 'Message',
    'list_submit' => 'Submit',
    'list_back' => 'back',
    'list_next' => 'next',

];

