@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="card card-default">
                <div class="card-body">
                <div class="card-title">{{ __('messages.index_menu') }}</div>
                    <ul>
                        <li><a href="/home/news" target="_blank">{{ __('messages.index_news') }}</a></li>
                        <li><a href="/home/contact" target="_blank">{{ __('messages.index_contact') }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
