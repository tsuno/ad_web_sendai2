@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
<?php foreach($list as $contact):?>
            <div class="card card-default">
                <div class="card-body">
                <div class="card-title"><?= $contact->name; ?>(<?= $contact->email; ?>)</div>
                <p>
<?php
if($contact->type == 0)
{
    echo __('messages.contact_type_service');
}
else if($contact->type == 1)
{
    echo __('messages.contact_type_recruit');
}
else
{
    echo __('messages.contact_type_other');
}

?>
                </p>
                <?= nl2br($contact->body); ?>
                <p class="text-right"><?= $contact->ip; ?></p>
                <p class="text-right"><?= $contact->ua; ?></p>
                <p class="text-right"><?= $contact->created_at; ?></p>
                </div>
            </div>
<?php endforeach; ?>
<div class="row">
<?php if($_GET['p'] > 0):?>
<a class="btn btn-danger col-md-6" href="/home/contact?p=<?= ($_GET['p'] - 1); ?>">{{ __('messages.contact_back') }}</a>
<?php else: ?>
<div class="col-md-6">&nbsp;</div>
<?php endif;?>
<?php if((($_GET['p'] + 1) * 20) - $total < 0):?>
<a class="btn btn-primary col-md-6" href="/home/contact?p=<?= ($_GET['p'] + 1); ?>">{{ __('messages.contact_next') }}</a>
</div>
<?php endif;?>
</div>
        </div>
    </div>
</div>
@endsection
