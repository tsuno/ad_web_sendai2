<!doctype html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AD {{ __('messages.corporation_type') }}</title>
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/main.css">
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="/"><span class="text-danger">A</span>D {{ __('messages.corporation_type') }}</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/news.php">{{ __('messages.title_news') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/bbs">{{ __('messages.title_bbs') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/company">{{ __('messages.title_company') }}</a>
            </li>
    </div>
</nav>
<div class="container">
    <h1 class="text-center">{{ __('messages.bbs_post') }}</h1>
    <p class="text-center">{{ __('messages.bbs_discription') }}</p>
    <form action="/bbs/submit" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="name">{{ __('messages.bbs_name') }}</label>
            <input type="text" name="name" class="form-control" id="name">
        </div>
        <div class="form-group">
            <label for="title">{{ __('messages.bbs_title') }}</label>
            <input type="text" name="title" class="form-control" id="title">
        </div>
        <div class="form-group">
            <label for="file">{{ __('messages.bbs_file') }}</label>
            <input type="file" name="file" class="form-control" id="file">
        </div>
        <div class="form-group">
            <label for="body">{{ __('messages.bbs_content') }}</label>
            <textarea class="form-control" id="body" rows="10" name="body"></textarea>
        </div>
        <button type="submit" class="btn btn-primary btn-block">{{ __('messages.bbs_sent') }}</button>
    </form>
</div>
<footer class="text-center">
    Copyright AD Inc. All Rights Reserved.
</footer>
</body>
<scrpt src="/js/bootstrap.js">
</html>
