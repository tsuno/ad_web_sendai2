<!doctype html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AD {{ __('messages.corporation_type') }}</title>
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/main.css">
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="/"><span class="text-danger">A</span>D {{ __('messages.corporation_type') }}</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/news.php">{{ __('messages.title_news') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/bbs">{{ __('messages.title_bbs') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/company">{{ __('messages.title_company') }}</a>
            </li>
    </div>
</nav>
<div class="container">
    <h1 class="text-center">{{ __('messages.title_bbs') }}</h1>
    <a href="/bbs/submit" class="btn btn-primary btn-block">{{ __('messages.list_to_post') }}</a>
    <div class="row">
        <div class="col-md-12">
            <?php foreach($list as $bbs):?>
            <div class="bbs">
                <h2><?= $bbs->title;?></h2>
                <p><?= nl2br($bbs->body); ?></p>
                <?php if($bbs->path != ''): ?>
                <p><img src="/files/<?= $bbs->path; ?>" width="200"></p>
                <?php endif;?>
                <p class="text-right"><?= $bbs->name;?>&nbsp;-&nbsp;<?= $bbs->created_at;?></p>
                <hr>
                <form action="/bbs/addtag" method="get">
                    <input type="hidden" name="p" value="<?= $_GET['p']; ?>">
                    <input type="hidden" name="id" value="<?= $bbs->id; ?>">
                    <button type="submit" class="btn btn-primary btn-block">{{ __('messages.list_add') }}</button>
                </form>
                <hr>
                <?php
                $sql = "SELECT * FROM comments WHERE bbs_id = {$bbs->id} ORDER BY id DESC;";
                $comments = \DB::select(\DB::raw($sql));
                if(count($comments) > 0): ?>
                <h3>{{ __('messages.list_comment_title') }}</h3>
                <?php endif; ?>
                <?php foreach($comments as $comment): ?>
                <div>
                    <p><?= nl2br($comment->body); ?></p>
                    <p class="text-right"><?= $comment->name;?>&nbsp;-&nbsp;<?= $comment->created_at;?></p>
                </div>
                <hr>
                <?php endforeach; ?>
                <h3>{{ __('messages.list_comment_post') }}</h3>
                <form method="post" action="/bbs/comment">
                    <input type="hidden" name="p" value="<?= $_GET['p']; ?>">
                    <input type="hidden" name="id" value="<?= $bbs->id; ?>">
                    <div class="form-group">
                        <label for="name">{{ __('messages.list_name') }}</label>
                        <input type="text" name="name" class="form-control" id="name">
                    </div>
                    <div class="form-group">
                        <label for="body">{{ __('messages.list_content') }}</label>
                        <textarea class="form-control" id="body" rows="3" name="body"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">{{ __('messages.list_submit') }}</button>
                </form>
            </div>
            <?php endforeach; ?>
            <div class="row">
                <?php if($_GET['p'] > 0):?>
                <a class="btn btn-danger col-md-6"
                   href="/bbs?p=<?= ($_GET['p'] - 1); ?>">{{ __('messages.list_back') }}</a>
                <?php else: ?>
                <div class="col-md-6">&nbsp;</div>
                <?php endif;?>
                <?php if((($_GET['p'] + 1) * 20) - $total < 0):?>
                <a class="btn btn-primary col-md-6"
                   href="/bbs?p=<?= ($_GET['p'] + 1); ?>">{{ __('messages.list_next') }}</a>
            </div>
            <?php endif;?>
        </div>
    </div>
</div>
</div>
<footer class="text-center">
    Copyright AD Inc. All Rights Reserved.
</footer>
</body>
<scrpt src="/js/bootstrap.js">
</html>
