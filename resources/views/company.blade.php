<!doctype html>
<html lang="ja">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>AD {{ __('messages.corporation_type') }}</title>
        <link rel="stylesheet" href="/css/bootstrap.css">
        <link rel="stylesheet" href="/css/main.css">
    </head>
    <body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
  <a class="navbar-brand" href="/"><span class="text-danger">A</span>D {{ __('messages.corporation_type') }}</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="/news.php">{{ __('messages.title_news') }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/bbs">{{ __('messages.title_bbs') }}</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="/company">{{ __('messages.title_company') }}</a>
      </li>
  </div>
</nav>
<div class="container">   
    <h1 class="text-center">{{ __('messages.title_company') }}</h1>
    <table class="table">
        <th>{{ __('messages.company_name_title') }}</th>
            <td>AD{{ __('messages.company_name') }}</td>
        </tr>
        <tr>
            <th>{{ __('messages.company_address_title') }}</th>
            <td>{{ __('messages.company_address') }}</td>
        </tr>
        <tr>
            <th>{{ __('messages.company_establishment_title') }}</th>
            <td>{{ __('messages.company_establishment') }}</td>
        </tr>
        <tr>
            <th>{{ __('messages.company_officer_title') }}</th>
            <td>{{ __('messages.company_officer') }}</td>
        </tr>
        <tr>
            <th>{{ __('messages.company_business_title') }}</th>
            <td>{{ __('messages.company_business') }}</td>
        </tr>
        <tr>
            <th>{{ __('messages.company_employees_title') }}</th>
            <td>{{ __('messages.company_employees') }}</td>
        </tr>
        <tr>
            <th>{{ __('messages.company_bank_title') }}</th>
            <td>{{ __('messages.company_bank') }}</td>
        </tr>
        <tr>
            <th>{{ __('messages.company_motto_title') }}</th>
            <td>{{ __('messages.company_motto') }}</td>
        </tr>
    </table>
    </div>
</div>
<footer class="text-center">
Copyright AD Inc. All Rights Reserved.
</footer>
    </body>
    <scrpt src="/js/bootstrap.js">
</html>
