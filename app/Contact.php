<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';

    public static function getTotal(){
        $sql = "SELECT count(id) as c FROM contacts;";
        return \DB::select(\DB::raw($sql))[0]->c;
    }

    public static function getList(){
        $min = 20 * $_GET['p'];
        $sql = "SELECT * FROM contacts ORDER BY id DESC LIMIT {$min}, 20;";
        return \DB::select(\DB::raw($sql));
    }
}
