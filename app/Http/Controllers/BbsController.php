<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bbs;

class BbsController extends Controller
{
    public function index(){

        if(!isset($_GET['p'])){
            $_GET['p'] = 0;
        }
        
        $list = Bbs::getList();
        return view('bbs.list', ['list' => $list, 'total' => Bbs::getTotal()]);
    }

    public function submit_form(){
        return view('bbs.submit');
    }

    public function submit(){
        $bbs = new Bbs();
        $bbs->insert();
        return redirect('/bbs');
    }

    public function comment(){
        
        $time = date("Y-m-d H:i:s");
        $sql = "INSERT INTO comments (bbs_id, name, body, ip, ua, created_at)";
        $sql .= " VALUES('{$_POST['id']}'";
        $sql .= ",'{$_POST['name']}'";
        $sql .= ",'{$_POST['body']}'";
        $sql .= ",'{$_SERVER['REMOTE_ADDR']}'";
        $sql .= ",'{$_SERVER['HTTP_USER_AGENT']}'";
        $sql .= ",'{$time}')";
        \DB::select(\DB::raw($sql));
        return redirect('/bbs?p=' . $_POST['p']);
    }
}
