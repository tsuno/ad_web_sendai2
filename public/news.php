<!doctype html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AD Inc.</title>
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/main.css">
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="/"><span class="text-danger">A</span>D Inc.</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/news.php">NEWS</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/bbs">BBS</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/company">Company Profile</a>
            </li>
    </div>
</nav>
<div class="container">
    <?php if (!isset($_GET['q'])): ?>
        <h1 class="text-center">NEWS</h1>
        <div class="news">
            <?php
            $dirs = explode("\n", shell_exec("ls news"));
            foreach ($dirs as $dir):
                ?>
                <h3 class="text-center"><a href="/news.php?q=<?= $dir; ?>"><?php echo urldecode(trim($dir)); ?></a></h3>
            <?php endforeach; ?>
        </div>
    <?php else: ?>
        <h1 class="text-center"><?= urldecode($_GET['q']); ?></h1>
        <div><?= nl2br(shell_exec("cat news/{$_GET['q']}")); ?></div>
    <?php endif; ?>
</div>
<footer class="text-center">
    Copyright AD Inc. All Rights Reserved.
</footer>
</body>
<scrpt src="/js/bootstrap.js">
</html>