<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $_GET['p'] = 0;
    $list = \App\Bbs::getList();
    return view('top',['list' => $list]);
});

Route::get('/company', function () {
    return view('company');
});

Route::get('bbs', 'BbsController@index');
Route::get('bbs/submit', 'BbsController@submit_form');
Route::post('bbs/submit', 'BbsController@submit');

Route::post('bbs/comment', 'BbsController@comment');

Auth::routes();
Route::get('/admin', 'AdminController@index');
Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/home/contact', 'HomeController@contact');
Route::get('/home/news', 'HomeController@news');
Route::post('/home/news', 'HomeController@news_submit');
Route::get('/home/news/delete/{id}', 'HomeController@news_delete');